# README #

This project is just a demo of how to use DbUp library in C#. 
It is expecting that the sql scripts are stored inside this project and when the script it is applied it stores the File Hash in db.
The next time, if the File Hash is different for the same file, then that script will be applied.

### More info on DbUp? ###

* [DbUp](https://dbup.readthedocs.io/en/latest/)