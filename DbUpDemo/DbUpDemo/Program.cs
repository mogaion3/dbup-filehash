﻿using DbUp;
using DbUpDemo.Helpers;
using System;
using System.Data;
using System.Linq;
using System.Reflection;

namespace DbUpDemo
{
    class Program
    {
        static int Main(string[] args)
        {
            var connectionString = "Server=(local); Database=DbUpDemo; Trusted_connection=true";

            var engineBuilder =
                DeployChanges.To
                    .SqlDatabase(connectionString)
                    .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                    .WithScriptNameComparer(new AscScriptNameComparer())
                    .LogToConsole()
                    .WithFilter(new FileHashScriptFilter());

            engineBuilder.Configure(x => x.Journal = new FileHashSqlTableJournal(() => x.ConnectionManager, () => x.Log, "dbo", "demoMigrationTable"));

            var arg = args.FirstOrDefault();

            switch (arg)
            {
                case "-T":
                case "-t":
                    engineBuilder = engineBuilder.WithTransactionAlwaysRollback();
                    break;
                case "-R":
                case "-r":
                    engineBuilder = engineBuilder.WithTransactionPerScript();
                    break;
                default:
                    throw new ApplicationException("You must specify the '-t' or the '-r' arguments.");
            }

            var upgrader = engineBuilder.Build();

            var upgraderScripts = upgrader.GetScriptsToExecute();
            Console.WriteLine($"These are the scripts to be executed: \n{string.Join("\n", upgraderScripts.Select(x => x.Name))}");

            var result = upgrader.PerformUpgrade();

            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
                Console.ResetColor();
#if DEBUG
                Console.ReadLine();
#endif
                return -1;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Success!");
            Console.ResetColor();
            return 0;
        }

        
    }
}
