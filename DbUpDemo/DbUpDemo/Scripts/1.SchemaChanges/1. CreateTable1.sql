﻿CREATE TABLE DemoDbUpTable
(
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[DemoID] [varchar](10) NULL,
	[ConfigurationOption] [varchar](50) NOT NULL,
	[Value] [varchar](250) NOT NULL,
	[LastModifiedByContId] [varchar](256) NOT NULL,
	[LastUpdate] [datetime] NULL,
 CONSTRAINT [pk_DemoDbUpTable] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
GO