﻿using DbUp.Engine;
using DbUp.Engine.Output;
using DbUp.Engine.Transactions;
using DbUp.SqlServer;
using System;
using System.Data;

namespace DbUpDemo.Helpers
{
    public class FileHashSqlTableJournal : SqlTableJournal
    {
        public FileHashSqlTableJournal(Func<IConnectionManager> connectionManager, Func<IUpgradeLog> logger, string schema, string table)
        : base(connectionManager, logger, schema, table)
        {
        }

        protected override string CreateSchemaTableSql(string quotedPrimaryKeyName)
        {
            return @$"create table {FqSchemaTableName} (    
                            [Id] int identity(1,1) not null constraint {quotedPrimaryKeyName} primary key,
                            [ScriptName] nvarchar(255) not null,
                            [ScriptVersion] nvarchar(255) not null,
                            [Applied] datetime2 not null)";
        }

        public override void StoreExecutedScript(SqlScript script, Func<IDbCommand> dbCommandFactory)
        {
            EnsureTableExistsAndIsLatestVersion(dbCommandFactory);
            using IDbCommand dbCommand = GetInsertScriptCommand(dbCommandFactory, script, FileHashingHelper.CreateFileVersionHash(script.Contents));
            dbCommand.ExecuteNonQuery();
        }

        protected override string GetJournalEntriesSql()
        {
            return $"select concat([ScriptName], '-', [ScriptVersion]) as name from {FqSchemaTableName} order by [ScriptName]";
        }

        protected IDbCommand GetInsertScriptCommand(Func<IDbCommand> dbCommandFactory, SqlScript script, string versionHash)
        {
            IDbCommand dbCommand = dbCommandFactory();
            IDbDataParameter dbDataParameter = dbCommand.CreateParameter();
            dbDataParameter.ParameterName = "scriptName";
            dbDataParameter.Value = script.Name;
            dbCommand.Parameters.Add(dbDataParameter);
            IDbDataParameter dbDataParameter2 = dbCommand.CreateParameter();
            dbDataParameter2.ParameterName = "applied";
            dbDataParameter2.Value = DateTime.Now;
            dbCommand.Parameters.Add(dbDataParameter2);
            IDbDataParameter dbDataParameter3 = dbCommand.CreateParameter();
            dbDataParameter3.ParameterName = "versionHash";
            dbDataParameter3.Value = versionHash;
            dbCommand.Parameters.Add(dbDataParameter3);
            dbCommand.CommandText = GetInsertJournalEntrySql("@scriptName", "@applied", "@versionHash");
            dbCommand.CommandType = CommandType.Text;
            return dbCommand;
        }

        protected string GetInsertJournalEntrySql(string scriptName, string applied, string versionHash)
        {
            return @$"insert into {FqSchemaTableName} (ScriptName, Applied, ScriptVersion) 
                      values ({scriptName}, {applied}, {versionHash})";
        }
    }
}
