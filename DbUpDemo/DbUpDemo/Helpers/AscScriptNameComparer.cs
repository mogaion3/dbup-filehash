﻿using System.Collections.Generic;

namespace DbUpDemo.Helpers
{
    public class AscScriptNameComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            return string.Compare(x, y);
        }
    }
}
