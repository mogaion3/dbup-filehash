﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace DbUpDemo.Helpers
{    
    public static class FileHashingHelper
    {
        public static string CreateFileVersionHash(string fileContent)
        {
            using var sha512 = SHA512.Create();
            var hashArray = sha512.ComputeHash(Encoding.ASCII.GetBytes(fileContent));
            return BitConverter.ToString(hashArray).Replace("-", "").ToLowerInvariant();    
        }
    }
}
