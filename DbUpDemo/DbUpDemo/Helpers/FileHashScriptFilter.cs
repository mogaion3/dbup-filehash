﻿using DbUp.Engine;
using DbUp.Support;
using System.Collections.Generic;
using System.Linq;

namespace DbUpDemo.Helpers
{
    public class FileHashScriptFilter : IScriptFilter
    {
        public IEnumerable<SqlScript> Filter(IEnumerable<SqlScript> sorted, HashSet<string> executedScriptNames, ScriptNameComparer comparer)
        {
            var scriptList = new List<SqlScript>(sorted.Count());
            foreach (var script in sorted.Where(x => !executedScriptNames.Contains(x.Name + "-" + FileHashingHelper.CreateFileVersionHash(x.Contents))))
                scriptList.Add(script);

            return scriptList;
        }
    }
}